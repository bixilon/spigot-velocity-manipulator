package de.bixilon.velocitymodifier;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;
import com.comphenix.protocol.events.PacketContainer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.lang.reflect.InvocationTargetException;

public class FakeVelocityCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (args.length != 4) {
            sender.sendMessage("§cUsage: /velocity <Entity id> <X> <Y> <Z>");
            return false;
        }
        if (!(sender instanceof Player)) {
            return false;
        }

        PacketContainer container = new PacketContainer(PacketType.Play.Server.ENTITY_VELOCITY);
        container.getIntegers().write(0, Integer.parseInt(args[0]));
        container.getIntegers()
                .write(1, (int) (Double.parseDouble(args[1]) * 8000))
                .write(2, (int) (Double.parseDouble(args[2]) * 8000))
                .write(3, (int) (Double.parseDouble(args[3]) * 8000));
        try {
            VelocityModifier.protocolManager.sendServerPacket((Player) sender,container);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        sender.sendMessage("Fake velocity sent!");
        return true;
    }

}
