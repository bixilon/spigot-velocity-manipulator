package de.bixilon.velocitymodifier;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.util.Vector;

public class VelocityCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (args.length != 4) {
            sender.sendMessage("§cUsage: /velocity <Entity id> <X> <Y> <Z>");
            return false;
        }
        Entity entity = getEntityById(Integer.parseInt(args[0]));
        if (entity == null) {
            sender.sendMessage("Unknown entity!");
            return false;
        }
        entity.setVelocity(new Vector(Double.parseDouble(args[1]), Double.parseDouble(args[2]), Double.parseDouble(args[3])));
        sender.sendMessage("Velocity set for: " + entity);
        return true;
    }

    public Entity getEntityById(int entityId){
        // ToDo: Why not a map?
        for(Entity entity : Bukkit.getWorlds().iterator().next().getEntities()){
            if(entity.getEntityId() == entityId){
                return entity;
            }
        }
        return null;
    }
}
