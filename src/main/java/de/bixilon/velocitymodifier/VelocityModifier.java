package de.bixilon.velocitymodifier;

import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;
import org.bukkit.plugin.java.JavaPlugin;

public final class VelocityModifier extends JavaPlugin {
    public static ProtocolManager protocolManager;

    @Override
    public void onEnable() {
        protocolManager = ProtocolLibrary.getProtocolManager();
        getCommand("velocity").setExecutor(new VelocityCommand());
        getCommand("fakevelocity").setExecutor(new FakeVelocityCommand());
    }
}
